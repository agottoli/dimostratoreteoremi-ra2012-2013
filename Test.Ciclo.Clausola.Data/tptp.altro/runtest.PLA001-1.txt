Reading file PLA001-1.p...
16 clauses.
Starting satisfiability proving... à la Otter with no ordering
/* info */
16 clauses from parsing, 
272 clauses generated, 
66 clauses subsumed, 
145 clauses in To_Select, 
75 clauses in Selected, 
in 1.371794278 seconds.
E` INSODDISFACIBILE, stampare la prova? [y,n]: 
Usare 'dot' per esportare un immagine del grafo della prova? [y,n]: 
Selezionare il formato di esportazione: [1=jpg, 2=ps, 3=pdf]: Grafo esportato in ./PLA001-1.pdf
Reading file PLA001-1.p...
16 clauses.
Starting satisfiability proving... à la Otter with 'standard' lexicographic ordering
/* info */
16 clauses from parsing, 
212 clauses generated, 
100 clauses subsumed, 
44 clauses in To_Select, 
82 clauses in Selected, 
in 0.67641479 seconds.
E` INSODDISFACIBILE, stampare la prova? [y,n]: 
Usare 'dot' per esportare un immagine del grafo della prova? [y,n]: 
Selezionare il formato di esportazione: [1=jpg, 2=ps, 3=pdf]: Grafo esportato in ./PLA001-1-lex-stP.pdf
Reading file PLA001-1.p...
16 clauses.
Starting satisfiability proving... à la Otter with 'standard' multiset ordering
/* info */
16 clauses from parsing, 
209 clauses generated, 
97 clauses subsumed, 
44 clauses in To_Select, 
82 clauses in Selected, 
in 0.813035443 seconds.
E` INSODDISFACIBILE, stampare la prova? [y,n]: 
Usare 'dot' per esportare un immagine del grafo della prova? [y,n]: 
Selezionare il formato di esportazione: [1=jpg, 2=ps, 3=pdf]: Grafo esportato in ./PLA001-1-mul-stP.pdf
Reading file PLA001-1.p...
16 clauses.
Starting satisfiability proving... à la Otter with 'standard' kbo ordering
/* info */
16 clauses from parsing, 
212 clauses generated, 
100 clauses subsumed, 
44 clauses in To_Select, 
82 clauses in Selected, 
in 0.718068829 seconds.
E` INSODDISFACIBILE, stampare la prova? [y,n]: 
Usare 'dot' per esportare un immagine del grafo della prova? [y,n]: 
Selezionare il formato di esportazione: [1=jpg, 2=ps, 3=pdf]: Grafo esportato in ./PLA001-1-kbo-stP.pdf
Reading file PLA001-1.p...
16 clauses.
Starting satisfiability proving... à la Otter with no ordering and set of support strategy
/* info */
16 clauses from parsing, 
117 clauses generated, 
56 clauses subsumed, 
24 clauses in To_Select, 
51 clauses in Selected, 
in 0.358022417 seconds.
E` INSODDISFACIBILE, stampare la prova? [y,n]: 
Usare 'dot' per esportare un immagine del grafo della prova? [y,n]: 
Selezionare il formato di esportazione: [1=jpg, 2=ps, 3=pdf]: Grafo esportato in ./PLA001-1-sos.pdf
Reading file PLA001-1.p...
16 clauses.
Starting satisfiability proving... à la Otter with 'standard' lexicographic ordering and set of support strategy
/* info */
16 clauses from parsing, 
489 clauses generated, 
259 clauses subsumed, 
103 clauses in To_Select, 
141 clauses in Selected, 
in 1.598221601 seconds.
E` INSODDISFACIBILE, stampare la prova? [y,n]: 
Usare 'dot' per esportare un immagine del grafo della prova? [y,n]: 
Selezionare il formato di esportazione: [1=jpg, 2=ps, 3=pdf]: Grafo esportato in ./PLA001-1-sos-lex-stP.pdf
Reading file PLA001-1.p...
16 clauses.
Starting satisfiability proving... à la Otter with 'standard' multiset ordering and set of support strategy
/* info */
16 clauses from parsing, 
486 clauses generated, 
256 clauses subsumed, 
103 clauses in To_Select, 
141 clauses in Selected, 
in 2.252717328 seconds.
E` INSODDISFACIBILE, stampare la prova? [y,n]: 
Usare 'dot' per esportare un immagine del grafo della prova? [y,n]: 
Selezionare il formato di esportazione: [1=jpg, 2=ps, 3=pdf]: Grafo esportato in ./PLA001-1-sos-mul-stP.pdf
Reading file PLA001-1.p...
16 clauses.
Starting satisfiability proving... à la Otter with 'standard' kbo ordering and set of support strategy
/* info */
16 clauses from parsing, 
489 clauses generated, 
259 clauses subsumed, 
103 clauses in To_Select, 
141 clauses in Selected, 
in 1.538767254 seconds.
E` INSODDISFACIBILE, stampare la prova? [y,n]: 
Usare 'dot' per esportare un immagine del grafo della prova? [y,n]: 
Selezionare il formato di esportazione: [1=jpg, 2=ps, 3=pdf]: Grafo esportato in ./PLA001-1-sos-kbo-stP.pdf
