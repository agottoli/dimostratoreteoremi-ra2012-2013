Reading file PUZ003-1.p...
8 clauses.
Starting satisfiability proving... à la Otter with no ordering
/* info */
8 clauses from parsing, 
89 clauses generated, 
35 clauses subsumed, 
27 clauses in To_Select, 
23 clauses in Selected, 
in 0.220398975 seconds.
E` INSODDISFACIBILE, stampare la prova? [y,n]: 
Usare 'dot' per esportare un immagine del grafo della prova? [y,n]: 
Selezionare il formato di esportazione: [1=jpg, 2=ps, 3=pdf]: Grafo esportato in ./PUZ003-1.pdf
Reading file PUZ003-1.p...
8 clauses.
Starting satisfiability proving... à la Otter with 'standard' lexicographic ordering
/* info */
8 clauses from parsing, 
53 clauses generated, 
20 clauses subsumed, 
12 clauses in To_Select, 
23 clauses in Selected, 
in 0.124952481 seconds.
E` INSODDISFACIBILE, stampare la prova? [y,n]: 
Usare 'dot' per esportare un immagine del grafo della prova? [y,n]: 
Selezionare il formato di esportazione: [1=jpg, 2=ps, 3=pdf]: Grafo esportato in ./PUZ003-1-lex-stP.pdf
Reading file PUZ003-1.p...
8 clauses.
Starting satisfiability proving... à la Otter with 'standard' multiset ordering
/* info */
8 clauses from parsing, 
53 clauses generated, 
20 clauses subsumed, 
12 clauses in To_Select, 
23 clauses in Selected, 
in 0.129664882 seconds.
E` INSODDISFACIBILE, stampare la prova? [y,n]: 
Usare 'dot' per esportare un immagine del grafo della prova? [y,n]: 
Selezionare il formato di esportazione: [1=jpg, 2=ps, 3=pdf]: Grafo esportato in ./PUZ003-1-mul-stP.pdf
Reading file PUZ003-1.p...
8 clauses.
Starting satisfiability proving... à la Otter with 'standard' kbo ordering
/* info */
8 clauses from parsing, 
23 clauses generated, 
11 clauses subsumed, 
6 clauses in To_Select, 
12 clauses in Selected, 
in 0.037136357 seconds.
E` INSODDISFACIBILE, stampare la prova? [y,n]: 
Usare 'dot' per esportare un immagine del grafo della prova? [y,n]: 
Selezionare il formato di esportazione: [1=jpg, 2=ps, 3=pdf]: Grafo esportato in ./PUZ003-1-kbo-stP.pdf
Reading file PUZ003-1.p...
8 clauses.
Starting satisfiability proving... à la Otter with no ordering and set of support strategy
/* info */
8 clauses from parsing, 
43 clauses generated, 
19 clauses subsumed, 
10 clauses in To_Select, 
20 clauses in Selected, 
in 0.100070477 seconds.
E` INSODDISFACIBILE, stampare la prova? [y,n]: 
Usare 'dot' per esportare un immagine del grafo della prova? [y,n]: 
Selezionare il formato di esportazione: [1=jpg, 2=ps, 3=pdf]: Grafo esportato in ./PUZ003-1-sos.pdf
Reading file PUZ003-1.p...
8 clauses.
Starting satisfiability proving... à la Otter with 'standard' lexicographic ordering and set of support strategy
/* info */
8 clauses from parsing, 
3 clauses generated, 
1 clauses subsumed, 
0 clauses in To_Select, 
10 clauses in Selected, 
in 0.007283469 seconds.
E` SODDISFACIBILE.
Reading file PUZ003-1.p...
8 clauses.
Starting satisfiability proving... à la Otter with 'standard' multiset ordering and set of support strategy
/* info */
8 clauses from parsing, 
3 clauses generated, 
1 clauses subsumed, 
0 clauses in To_Select, 
10 clauses in Selected, 
in 0.010091645 seconds.
E` SODDISFACIBILE.
Reading file PUZ003-1.p...
8 clauses.
Starting satisfiability proving... à la Otter with 'standard' kbo ordering and set of support strategy
/* info */
8 clauses from parsing, 
15 clauses generated, 
8 clauses subsumed, 
1 clauses in To_Select, 
12 clauses in Selected, 
in 0.023779563 seconds.
E` INSODDISFACIBILE, stampare la prova? [y,n]: 
Usare 'dot' per esportare un immagine del grafo della prova? [y,n]: 
Selezionare il formato di esportazione: [1=jpg, 2=ps, 3=pdf]: Grafo esportato in ./PUZ003-1-sos-kbo-stP.pdf
