Reading file ALG002-1.p...
14 clauses.
Starting satisfiability proving... à la Otter with no ordering
/* info */
14 clauses from parsing, 
27062 clauses generated, 
14975 clauses subsumed, 
11068 clauses in To_Select, 
1012 clauses in Selected, 
in 300.091005645 seconds (out of time limit).
E` SODDISFACIBILE.
Reading file ALG002-1.p...
14 clauses.
Starting satisfiability proving... à la Otter with 'standard' lexicographic ordering
/* info */
14 clauses from parsing, 
32874 clauses generated, 
22309 clauses subsumed, 
9843 clauses in To_Select, 
736 clauses in Selected, 
in 302.843652816 seconds (out of time limit).
E` SODDISFACIBILE.
Reading file ALG002-1.p...
14 clauses.
Starting satisfiability proving... à la Otter with 'standard' multiset ordering
/* info */
14 clauses from parsing, 
33690 clauses generated, 
23008 clauses subsumed, 
10094 clauses in To_Select, 
602 clauses in Selected, 
in 302.670357183 seconds (out of time limit).
E` SODDISFACIBILE.
Reading file ALG002-1.p...
14 clauses.
Starting satisfiability proving... à la Otter with 'standard' kbo ordering
/* info */
14 clauses from parsing, 
131101 clauses generated, 
124811 clauses subsumed, 
4964 clauses in To_Select, 
1340 clauses in Selected, 
in 300.340594088 seconds (out of time limit).
E` SODDISFACIBILE.
Reading file ALG002-1.p...
14 clauses.
Starting satisfiability proving... à la Otter with no ordering and set of support strategy
/* info */
14 clauses from parsing, 
962 clauses generated, 
656 clauses subsumed, 
208 clauses in To_Select, 
108 clauses in Selected, 
in 2.408006001 seconds.
E` INSODDISFACIBILE, stampare la prova? [y,n]: 
Usare 'dot' per esportare un immagine del grafo della prova? [y,n]: 
Selezionare il formato di esportazione: [1=jpg, 2=ps, 3=pdf]: Grafo esportato in ./ALG002-1-sos.pdf
Reading file ALG002-1.p...
14 clauses.
Starting satisfiability proving... à la Otter with 'standard' lexicographic ordering and set of support strategy
/* info */
14 clauses from parsing, 
1 clauses generated, 
1 clauses subsumed, 
0 clauses in To_Select, 
14 clauses in Selected, 
in 0.006124378 seconds.
E` SODDISFACIBILE.
Reading file ALG002-1.p...
14 clauses.
Starting satisfiability proving... à la Otter with 'standard' multiset ordering and set of support strategy
/* info */
14 clauses from parsing, 
1 clauses generated, 
1 clauses subsumed, 
0 clauses in To_Select, 
14 clauses in Selected, 
in 0.014800974 seconds.
E` SODDISFACIBILE.
Reading file ALG002-1.p...
14 clauses.
Starting satisfiability proving... à la Otter with 'standard' kbo ordering and set of support strategy
/* info */
14 clauses from parsing, 
0 clauses generated, 
0 clauses subsumed, 
0 clauses in To_Select, 
14 clauses in Selected, 
in 0.003851537 seconds.
E` SODDISFACIBILE.
